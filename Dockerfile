FROM registry.gitlab.com/dedyms/node:14-dev AS tukang
RUN git clone --depth=1 https://github.com/dockersamples/docker-swarm-visualizer.git /docker-swarm-visualizer
WORKDIR /docker-swarm-visualizer
RUN npm ci && npm run dist

FROM registry.gitlab.com/dedyms/node:14
ENV MS=500 CTX_ROOT=/
COPY --from=tukang /docker-swarm-visualizer /docker-swarm-visualizer
WORKDIR /docker-swarm-visualizer
HEALTHCHECK CMD node /docker-swarm-visualizer/healthcheck.js || exit 1
CMD ["node", "server.js"]


